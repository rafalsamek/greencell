<?php
/**
 * Created by PhpStorm.
 * User: rafal
 * Date: 14.06.18
 * Time: 01:46
 */

namespace GroupInColumns;


class GroupInColumns
{

    /**
     * @param array $input
     * @param int $rows
     * @param int $columns
     * @return array
     */
    public static function groupInColumns(array $input, int &$rows = 0, int $columns = 4)
    {
        /**
         * Calculate average length of column
         * -3 because the total length is reduced by $columns -1 number of unneeded empty lines - group delimiters
         */
        $avgLength = (count($input) - ($columns - 1) ) / $columns;
        /**
         * First implode by , then explode by ,, and iterate over the result to explode by ,
         */
        $input = implode(',', $input);
        $input = explode(',,', $input);
        $result = [];
        foreach ($input as $item) {
            $subResult = explode(',', $item);
            $result[$subResult[0]] = $subResult;
        }

        /**
         * Sort the array by keys string case insensitive
         */
        uksort($result, 'strcasecmp');

        /**
         * Initialize the $output array
         */
        $output = [];
        for($i = 0; $i < $columns; $i++) {
            $output[$i] = [];
        }

        /**
         * Initialize $row and $column with zeros
         */
        $row = 0;
        $column = 0;
        $rows = 0;

        foreach($result as $value) {
            /**
             * This is the key condition
             * We decide if to append group or not to the column
             * If distance between column with appended group and $avgLength
             * is not greater then distance between $avgLength and column without appended group
             */
            if($row+1 + count($value) - $avgLength > $avgLength - $row) {
                $column++;
                $row = 0;
            }
            /**
             * Put empty row if column is not empty
             */
            if ($row > 0) {
                $output[$column][$row] = '';
                $row++;
            }
            foreach ($value as $val) {
                $output[$column][$row] = $val;
                $row++;
                if($row > $rows) {
                    $rows = $row;
                }
            }
        }
        return $output;
    }
}
?>
<html>
<head>
    <title>Green Cell - zadanie rekrutacyjne</title>
    <style>
        table {
            border-collapse: collapse;
        }

        td {
            border: 1px solid black;
            width: 150px;
            height: 40px;
            padding: 10px 20px;
        }
    </style>
</head>
    <form method="POST" action="">
        <div>
            <label for="inputTextarea">Input: </label>
            <textarea name="input" id="inputTextarea" rows="20" cols="50"">
Acrobat
Acrobat
Acrobat
Acrobat
Acrobat
Acrobat
Acrobat

CS
CS
CS
CS
CS

Captivate

ColdFusion
ColdFusion
ColdFusion

ColdBuilder

Contribute
Contribute

eLearning

Director
Director
Director
Director
Director
Director
Director
Director

Fireworks

Flash
Flash
Flash
Flash
Flash

FontFolio
FontFolio
FontFolio
FontFolio
FontFolio

Freehand
Freehand
Freehand

InDesign
InDesign

Lightroom

PageMaker
PageMaker
PageMaker
PageMaker
PageMaker

Premiere
Premiere
Premiere
Premiere
Premiere</textarea>
        </div>
        <div>
            <input type="submit" value="Grupuj" style="cursor: pointer;"/>
        </div>
    </form>

<?php
if(!empty($_POST['input'])) {
    echo '<table id="results">';
    $in = explode("\n", $_POST['input']);
    $input = [];
    foreach($in as $item) {
        $input[] = trim($item);
    }

    $GroupInColumns = new \GroupInColumns\GroupInColumns();
    $rows = 0;
    $columns = 4;
    $output = $GroupInColumns::groupInColumns($input, $rows, $columns);

    for($row = 0; $row < $rows; $row++) {
        echo '<tr>';
        for($column = 0; $column < $columns; $column++) {
            echo '<td>' . $output[$column][$row] . '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
}
?>
</html>