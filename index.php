<?php
/**
 * Created by PhpStorm.
 * User: rafal
 * Date: 17.06.18
 * Time: 04:06
 */

?>
<html>
<head>
    <title>Green Cell - zadanie rekrutacyjne</title>
    <style>
        table {
            border-collapse: collapse;
        }

        td {
            border: 1px solid black;
            width: 150px;
            height: 40px;
            padding: 10px 20px;
        }
    </style>
</head>
    <form method="POST" action="">
        <div>
            <label for="inputTextarea">Input: </label>
            <textarea name="input" id="inputTextarea" rows="20" cols="50"">
Acrobat
Acrobat
Acrobat
Acrobat
Acrobat
Acrobat
Acrobat

CS
CS
CS
CS
CS

Captivate

ColdFusion
ColdFusion
ColdFusion

ColdBuilder

Contribute
Contribute

eLearning

Director
Director
Director
Director
Director
Director
Director
Director

Fireworks

Flash
Flash
Flash
Flash
Flash

FontFolio
FontFolio
FontFolio
FontFolio
FontFolio

Freehand
Freehand
Freehand

InDesign
InDesign

Lightroom

PageMaker
PageMaker
PageMaker
PageMaker
PageMaker

Premiere
Premiere
Premiere
Premiere
Premiere</textarea>
        </div>
        <div>
            <input type="submit" value="Grupuj" style="cursor: pointer;"/>
        </div>
    </form>

<?php
if(!empty($_POST['input'])) {
    echo '<table id="results">';
    $in = explode("\n", $_POST['input']);
    $input = [];
    foreach($in as $item) {
        $input[] = trim($item);
    }
    require_once "src/GroupInColumns.php";
    $GroupInColumns = new \GroupInColumns\GroupInColumns();
    $rows = 0;
    $columns = 4;
    $output = $GroupInColumns::groupInColumns($input, $rows, $columns);

    for($row = 0; $row < $rows; $row++) {
        echo '<tr>';
        for($column = 0; $column < $columns; $column++) {
            echo '<td>' . $output[$column][$row] . '</td>';
        }
        echo '</tr>';
    }
    echo '</table>';
}
?>
</html>