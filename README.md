####Dane wejściowe:
Pogrupowane kategorie produktów – jedna grupa to wszystkie kategorie o nazwie „Acrobat”
```
Acrobat
Acrobat
Acrobat
Acrobat
Acrobat
Acrobat
Acrobat

CS
CS
CS
CS
CS

Captivate

ColdFusion
ColdFusion
ColdFusion

ColdBuilder

Contribute
Contribute

eLearning

Director
Director
Director
Director
Director
Director
Director
Director

Fireworks

Flash
Flash
Flash
Flash
Flash

FontFolio
FontFolio
FontFolio
FontFolio
FontFolio

Freehand
Freehand
Freehand

InDesign
InDesign

Lightroom

PageMaker
PageMaker
PageMaker
PageMaker
PageMaker

Premiere
Premiere
Premiere
Premiere
Premiere
```

####Zadanie:
Dane wejściowe należy rozbić na 4 kolumny:
* zachowując porządek alfabetyczny grup,
* nie rozrywając grup na 2 lub więcej kolumn – jedna grupa musi w całości zmieścić się w jednej
kolumnie,
* wstawiając między grupami jedną linię pustą (po ostatniej kategorii w kolumnie nie trzeba
wstawiać pustej linii).
Problem:
Kategorie w grupach należy rozbić na 4 kolumny, tak aby te kolumny były jak najbardziej
zrównoważone pod względem ilości znajdujących się w nich kategorii( i odstępów pomiędzy nimi),
czyli dążymy do tego aby kolumny były jak najkrótsze.
Kryteria oceny:
* uzyskanie skryptu generującego najoptymalniejsze wyniki dla różnych danych wejściowych
* czas nadesłania odpowiedzi ‐ ostateczny termin nadsyłania odpowiedzi upływa w niedziele, 20
maja 2017 o godz. 19:00
* złożoność i przejrzystość kodu

Proszę o przesłanie rozwiązania w postaci jednoplikowego skryptu php zawierającego formularz z
polem textarea, do którego będzie można wkleić np. podane wyżej dane wejściowe i po wysłaniu 
formularza otrzyma się wynik zadania – kategorie w grupach podzielone na 4 kolumny.

Odpowiedzi proszę przesłać na adres rekrutacja@greencell.global


####Przykład:
######Dane wejściowe:
```
A1

B1
B2

C1
C2

D1
D2
D3

E1
E2
```
######Oczekiwany wynik:

```
|---------------|---------------|---------------|---------------|
| A1            | C1            | D1            | E1            |
|---------------|---------------|---------------|---------------|
|               | C2            | D2            | E2            |
|---------------|---------------|---------------|---------------|
| B1            |               | D3            |               |
|---------------|---------------|---------------|---------------|
| B2            |               |               |               |
|---------------|---------------|---------------|---------------|
```

